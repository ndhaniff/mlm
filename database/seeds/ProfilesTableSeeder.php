<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'user_id' => 1,
            'first_name' => "Endy",
            'last_name' => "Haniff",
            'gender' => "Male",
            'avatar' => "",
            'facebook_profile' => "",
            'twitter_profile' => "",
            'google_plus_profile' => "",
        ]);

        DB::table('profiles')->insert([
            'user_id' => 2,
            'first_name' => "John",
            'last_name' => "Doe",
            'gender' => "Male",
            'avatar' => "",
            'facebook_profile' => "",
            'twitter_profile' => "",
            'google_plus_profile' => "",
        ]);

        DB::table('profiles')->insert([
            'user_id' => 3,
            'first_name' => "Minah",
            'last_name' => "Doe",
            'gender' => "Female",
            'avatar' => "",
            'facebook_profile' => "",
            'twitter_profile' => "",
            'google_plus_profile' => "",
        ]);

    }
}
