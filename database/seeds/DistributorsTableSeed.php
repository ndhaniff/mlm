<?php

use Illuminate\Database\Seeder;
use App\Model\Distributor;

class DistributorsTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $distributor = new Distributor();
        $distributor->distributor_id = 'MLM-5b99c';
        $distributor->user_id = 1;
        $distributor->name = 'test';
        $distributor->email = 'test@yahoo.com';
        $distributor->phone_number = '016-16161616'; 
        $distributor->id_passport = '909090-90-9090';
        $distributor->nationality = 'Malaysian';
        $distributor->package_id = 1;
        $distributor->upline_distributor_id = 1; 
        $distributor->upline_path = 1;
        $distributor->status = 'pending approval';
        $distributor->approval_user_id = 1;
        $distributor->save();
    }
}
