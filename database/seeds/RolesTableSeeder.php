<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role(); 
        $role_admin->name = "Admin"; 
        $role_admin->description = "An administrator"; 
        $role_admin->save();

        $role_distributor = new Role(); 
        $role_distributor->name = "Distributor"; 
        $role_distributor->description = "A Distributor"; 
        $role_distributor->save();
        
        $role_director = new Role(); 
        $role_director->name = "Director"; 
        $role_director->description = "A Director"; 
        $role_director->save();
        
        $role_developer = new Role(); 
        $role_developer->name = "Developer"; 
        $role_developer->description = "A Developer"; 
        $role_developer->save();
    }
}
