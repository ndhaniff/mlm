<?php

use Illuminate\Database\Seeder;
use App\Model\WalletWithdrawal;

class WalletsWithdrawalSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $walletWithdrawal = new WalletWithdrawal();
        $walletWithdrawal->distributor_wallet_id = 1;
        $walletWithdrawal->amount = 400;
        $walletWithdrawal->user_id = 1;
        $walletWithdrawal->distributor_id = 'MLM-5b99c';
        $walletWithdrawal->status = 'pending';
        $walletWithdrawal->approval_user_id = 1;
        $walletWithdrawal->approval_comment = 'this transaction is goddamn legit';
        $walletWithdrawal->save();
    }
}
