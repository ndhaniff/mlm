<?php

use Illuminate\Database\Seeder;
use App\Model\Package;

class PackagesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $package = new Package();
        $package->name = 'Gold';
        $package->comission_point = 0.3;
        $package->comission_level = 2;
        $package->status = 'available';
        $package->save();
    }
}
