<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'), 
            'status' => 'activated'
        ]);

        DB::table('users')->insert([
            'email' => 'distributor@gmail.com',
            'password' => bcrypt('secret'), 
            'status' => 'activated'
        ]);

        DB::table('users')->insert([
            'email' => 'director@gmail.com',
            'password' => bcrypt('secret'), 
            'status' => 'banned'
        ]);

        DB::table('user_role')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);

        DB::table('user_role')->insert([
            'user_id' => 2,
            'role_id' => 2
        ]);

        DB::table('user_role')->insert([
            'user_id' => 3,
            'role_id' => 3
        ]);
    }
}
