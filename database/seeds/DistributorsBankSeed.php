<?php

use Illuminate\Database\Seeder;
use App\Model\DistributorsBank;

class DistributorsBankSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $distributor_bank = new DistributorsBank();
        $distributor_bank->distributor_id = 'MLM-5b99c';
        $distributor_bank->bank_name = 'Maybank';
        $distributor_bank->account_number = '7620445590';
        $distributor_bank->beneficiary_name = 'Mohd Mat Salleh';
        $distributor_bank->beneficiary_email ='matsalleh@gmail.com';
        $distributor_bank->beneficiary_phone = '011-42424242';
        $distributor_bank->save();
    }
}
