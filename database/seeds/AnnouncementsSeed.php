<?php

use Illuminate\Database\Seeder;
use App\Model\Announcement;

class AnnouncementsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $announcement = new Announcement();
        $announcement->title = 'Happy October';
        $announcement->content = 'Here on october we have special promotion just for you';
        $announcement->user_id = 1;
        $announcement->save();
    }
}
