<?php

use App\Model\Wallet;
use Illuminate\Database\Seeder;

class WalletsTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wallet = new Wallet();
        $wallet->name = 'PV Points';
        $wallet->description = 'PV Points';
        $wallet->icon = null;
        $wallet->withdrawable = 1;
        $wallet->withdraw_approval = 'approved';
        $wallet->min_withdraw = 100;
        $wallet->max_withdraw = 300;
        $wallet->transferable = 1;
        $wallet->transfer_approval = 'approved';
        $wallet->transferable_cross_distributor = 1;
        $wallet->min_transfer = 10;
        $wallet->max_transfer = 70;
        $wallet->save();

        $wallet2 = new Wallet();
        $wallet2->name = 'e Cash';
        $wallet2->description = 'e Cash';
        $wallet2->icon = null;
        $wallet2->withdrawable = 1;
        $wallet2->withdraw_approval = 'approved';
        $wallet2->min_withdraw = 100;
        $wallet2->max_withdraw = 300;
        $wallet2->transferable = 1;
        $wallet2->transfer_approval = 'approved';
        $wallet2->transferable_cross_distributor = 1;
        $wallet2->min_transfer = 10;
        $wallet2->max_transfer = 70;
        $wallet2->save();

        $wallet3 = new Wallet();
        $wallet3->name = 'Rewards Link';
        $wallet3->description = 'e Cash';
        $wallet3->icon = null;
        $wallet3->withdrawable = 1;
        $wallet3->withdraw_approval = 'approved';
        $wallet3->min_withdraw = 100;
        $wallet3->max_withdraw = 300;
        $wallet3->transferable = 1;
        $wallet3->transfer_approval = 'approved';
        $wallet3->transferable_cross_distributor = 1;
        $wallet3->min_transfer = 10;
        $wallet3->max_transfer = 70;
        $wallet3->save();

        DB::table('distributors_wallets')->insert([
            'wallet_id' => 1,
            'user_id' => 1,
            'distributor_id' => 1,
        ]);

        DB::table('distributors_wallets')->insert([
            'wallet_id' => 2,
            'user_id' => 1,
            'distributor_id' => 1,
        ]);

        DB::table('distributors_wallets')->insert([
            'wallet_id' => 3,
            'user_id' => 1,
            'distributor_id' => 1,
        ]);
    }
}
