<?php

use Illuminate\Database\Seeder;
use App\Model\WalletTransfer;

class WalletsTransferSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $walletTransfer = new WalletTransfer();
        $walletTransfer->distributor_wallet_id_source = 1;
        $walletTransfer->distributor_wallet_id_destination = 2;
        $walletTransfer->distributor_id_source = 'MLM-5b99c';
        $walletTransfer->distributor_id_destination = 'MLM-5b11x';
        $walletTransfer->amount_source = '300';
        $walletTransfer->amount_destination = '100';
        $walletTransfer->user_id = 1;
        $walletTransfer->remark = 'transfer to downline';
        $walletTransfer->status = 'pending';
        $walletTransfer->approval_user_id = 1;
        $walletTransfer->approval_comment = 'this guy is legit';
        $walletTransfer->save();
    }
}
