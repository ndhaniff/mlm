<?php

use Illuminate\Database\Seeder;
use App\Model\WalletTransactionHistory;

class WalletsTransactionHistorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $walletTransactionHistory = new WalletTransactionHistory();
        $walletTransactionHistory->distributor_wallet_id = 1;
        $walletTransactionHistory->type = 'debit';
        $walletTransactionHistory->amount = 300;
        $walletTransactionHistory->distributor_id = 1;
        $walletTransactionHistory->description = 'Referal Bonus';
        $walletTransactionHistory->save();
    }
}
