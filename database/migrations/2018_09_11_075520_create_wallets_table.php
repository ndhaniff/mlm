<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('icon')->nullable();
            $table->integer('withdrawable');
            $table->string('withdraw_approval');
            $table->integer('min_withdraw');
            $table->integer('max_withdraw');
            $table->integer('transferable');
            $table->string('transfer_approval');
            $table->integer('transferable_cross_distributor');
            $table->integer('min_transfer');
            $table->integer('max_transfer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
