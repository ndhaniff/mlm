<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets_transfer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('distributor_wallet_id_source');
            $table->string('distributor_wallet_id_destination');
            $table->string('distributor_wallet_name_source');
            $table->string('distributor_wallet_name_destination');
            $table->string('distributor_id_source');
            $table->string('distributor_id_destination');
            $table->string('amount_source');
            $table->string('amount_destination');
            $table->integer('user_id')->nullable();
            $table->string('remark')->nullable();
            $table->string('status');
            $table->integer('approval_user_id')->nullable();
            $table->string('approval_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets_transfer');
    }
}
