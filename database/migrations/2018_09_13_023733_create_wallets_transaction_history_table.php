<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTransactionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets_transaction_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_wallet_id')->nullable();
            $table->string('distributor_wallet_name')->nullable();
            $table->string('type');
            $table->string('amount');
            $table->string('distributor_id');
            $table->string('status')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets_transaction_history');
    }
}
