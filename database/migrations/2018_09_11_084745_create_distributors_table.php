<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('distributor_id')->unique();
            $table->integer('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('phone_number');
            $table->string('id_passport');
            $table->string('nationality');
            $table->integer('package_id');
            $table->string('upline_distributor_id');
            $table->string('before_distributor_id');
            $table->string('upline_path');
            $table->string('distributor_path');
            $table->string('status');
            $table->integer('approval_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributors');
    }
}
