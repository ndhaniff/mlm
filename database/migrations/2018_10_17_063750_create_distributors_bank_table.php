<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributorsBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('distributor_id');
            $table->string('bank_name');
            $table->string('account_number');
            $table->string('beneficiary_name');
            $table->string('beneficiary_email');
            $table->string('beneficiary_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributors_bank');
    }
}
