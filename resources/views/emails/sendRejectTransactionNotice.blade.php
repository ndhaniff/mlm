<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
  <div>
  <p>Your transaction has been rejected on {{date("F j, Y")}}</p>
    <br>
  <div>Reason: {{$description}}</div>
    <br>
    <small>Powered by <a href="http://tinnolab.com.my" target="_blank">Tinnolab</a></small>
  </div>
</body>
</html>