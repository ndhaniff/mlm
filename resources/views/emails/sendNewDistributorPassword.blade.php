<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
  <div>
  <p>{{$sender}} has added you as his/her downline on {{date("F j, Y")}}</p>
    <br>
    <table style="border:solid 1px">
      <tbody>
        <tr><td>Your Upline: {{$sender}}</td></tr>
        <tr><td>Your Account Email: {{$receiver}}</td></tr>
        <tr><td>Your password: {{$password}}</td></tr>
      </tbody>
    </table>
    <br>
    <small>Powered by <a href="http://tinnolab.com.my" target="_blank">Tinnolab</a></small>
  </div>
</body>
</html>