import {required,numeric,helpers, minLength, email} from "vuelidate/lib/validators/"
const alpha = helpers.regex('alpha', /D[a-zA-Z0-9]{3}$/g)

export default {
    personaldetails:{
      fullname: {
        required,
        minLength: minLength(6)
      },
      email: {
        required,
        email
      },
      contact: {
        required,
        numeric
      },
      icnum: {
        required
      },
      nationality: {
        required
      },
    },
    packdetails: {
      package: {
        required
      },
      directupline: {
        required,
        alpha
      },
    },
    bankdetails: {
      bankname: {
        required
      },
      accnumber: {
        required,
        numeric
      },
      beneficiaryname: {
        required,
        minLength:minLength(6)
      },
      email: {
        required,
        email
      },
      contact: {
        required
      }
    }
}