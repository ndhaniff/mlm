import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

const store = new Vuex.Store({
	state: {
		auth: {
			userId: '',
			first_name: '',
			last_name: '',
			email: '',
			avatar: '',
			access_token: ''
		},
		userData: {},
		chartData: null,
		config: {
			company_name: '',
			contact_person: ''
		},
		wallets:{},
	},
	mutations: {
		setAuthUserDetail (state, auth) {
        	for (let key of Object.keys(auth)) {
                state.auth[key] = auth[key];
			}
            if ('avatar' in auth)
            	state.auth.avatar = auth.avatar !== null ? auth.avatar : 'avatar.png';
		},
		resetAuthUserDetail (state) {
        	for (let key of Object.keys(state.auth)) {
                state.auth[key] = '';
            }
		},
		setConfig (state, config) {
        	for (let key of Object.keys(config)) {
                state.config[key] = config[key];
            }
		},
		setUserData (state, payload) {
			state.userData = payload;
		},
		setWalletsData( state, payload) {
			state.wallets = payload
		},
		setChartData( state, payload) {
			state.chartData = payload
		}
	},
	actions: {
		setAuthUserDetail ({ commit }, auth) {
     		commit('setAuthUserDetail',auth);
     	},
    	resetAuthUserDetail ({commit}) {
     		commit('resetAuthUserDetail');
     	},
		setConfig ({ commit } , data) {
     		commit('setConfig',data);
		 },
		setUserData ({ commit }, data) {
			commit('setUserData', data);
		},
		setWalletsData ({ commit }, data) {
			commit('setWalletsData', data)
		},
		setChartData ({commit}, data) {
			commit('setChartData', data)
		}
	},
	getters: {
		getAuthUser: (state) => (name) => {
		    return state.auth[name];
		},
		getAuthUserFullName: (state) => {
		    return state.auth['first_name']+' '+state.auth['last_name'];
		},
		getConfig: (state) => (name) => {
		    return state.config[name];
		},
		getUserData: (state) => {
			return state.userData;
		},
		getUser: (state) => (name) => {
			return state.userData[name];
		},
		getWalletsData: (state) => {
			return state.wallets
		},
		getChartData: (state) => {
			return state.chartData
		}
	},
	plugins: [
		createPersistedState({ storage: window.sessionStorage })
	]
});

export default store;