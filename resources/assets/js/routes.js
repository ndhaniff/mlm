import VueRouter from "vue-router";
import helper from "./services/helper";
import store from "./store";

let role;

role = store.getters.getUserData.role;

// ROUTE GUARDS //
const adminGuard = (to, from, next) => {
  if (role == undefined) {
    role = store.getters.getUserData.role;
  }
  if (role != "Admin") {
    toastr["error"]("You don't have enough permission");
    return next({
      path: "/"
    });
  }
  return next();
};

const distributorGuard = (to, from, next) => {
  if (role == undefined) {
    role = store.getters.getUserData.role;
  }
  if (role != "Distributor") {
    toastr["error"]("You don't have enough permission");
    return next({
      path: "/"
    });
  }
  return next();
};

const shareGuard = (to, from, next) => {
  if (role == undefined) {
    role = store.getters.getUserData.role;
  }
  if (role != "Distributor" && role != "Admin") {
    toastr["error"]("You don't have enough permission");
    return next({
      path: "/"
    });
  }
  return next();
};

let routes = [{
    path: "/",
    component: require("./layouts/default-page"),
    meta: {
      requiresAuth: true
    },
    children: [{
        path: "/",
        component: require("./views/pages/home")
      },
      {
        path: "/home",
        component: require("./views/pages/home")
      },
      {
        path: "/blank",
        component: require("./views/pages/blank")
      },
      {
        path: "/pending-distributor",
        component: require("./views/pending/pendingDistributor"),
        beforeEnter: adminGuard
      },
      {
        path: "/pending-distributor/edit/:id",
        component: require("./views/pending/components/EditDistributor"),
        beforeEnter: adminGuard
      },
      {
        path: "/pending-withdrawal",
        component: require("./views/pending/pendingWithdrawal"),
        beforeEnter: adminGuard
      },
      {
        path: "/pending-transfer",
        component: require("./views/pending/pendingTransfer"),
        beforeEnter: adminGuard
      },
      {
        path: "/withdrawal-request",
        component: require("./views/withdrawal/request"),
        beforeEnter: adminGuard
      },
      {
        path: "/withdrawal-request/list/:status",
        component: require("./views/withdrawal/approve"),
        beforeEnter: adminGuard
      },
      {
        path: "/withdrawal-add",
        component: require("./views/withdrawal/add"),
        beforeEnter: adminGuard
      },
      {
        path: "/transfer-request",
        component: require("./views/transfer/request"),
        beforeEnter: adminGuard
      },
      {
        path: "/transfer-request/list/:status",
        component: require("./views/transfer/approve"),
        beforeEnter: adminGuard
      },
      {
        path: "/transfer-add",
        component: require("./views/transfer/request"),
        beforeEnter: adminGuard
      },
      {
        path: "/announcement",
        component: require("./views/announcement/list"),
        beforeEnter: adminGuard
      },
      {
        path: "/distributor",
        component: require("./views/distributor/list"),
        beforeEnter: adminGuard
      },
      {
        path: "/distributor-add",
        component: require("./views/distributor/add"),
        beforeEnter: shareGuard
      },
      {
        path: "/distributor-profile",
        component: require("./views/distributor/profile"),
        beforeEnter: distributorGuard
      },
      {
        path: "/genealogy",
        component: require("./views/genealogy/genealogy"),
        beforeEnter: distributorGuard
      },
      {
        path: "/account",
        component: require("./views/auth/account")
      },
      {
        path: "/wallet",
        component: require("./views/distributor/wallet"),
        beforeEnter: distributorGuard
      },
      {
        path: "/pvpoints",
        component: require("./views/distributor/pages/pvpoints"),
        beforeEnter: distributorGuard
      },
      {
        path: "/ecash",
        component: require("./views/distributor/pages/ecash"),
        beforeEnter: distributorGuard
      },
      {
        path: "/rewardspoints",
        component: require("./views/distributor/pages/rewardspoints"),
        beforeEnter: distributorGuard
      },
      {
        path: "/withdraw/:wallet?",
        component: require("./views/distributor/pages/withdraw"),
        beforeEnter: distributorGuard
      },
      {
        path: "/transfer/:wallet?",
        component: require("./views/distributor/pages/transfer"),
        beforeEnter: distributorGuard
      },
      {
        path: "/configuration",
        component: require("./views/configuration/configuration")
      },
      {
        path: "/profile",
        component: require("./views/user/profile")
      },
      {
        path: "/task",
        component: require("./views/task/index")
      },
      {
        path: "/task/:id/edit",
        component: require("./views/task/edit")
      },
      {
        path: "/user",
        component: require("./views/user/index")
      }
    ]
  },
  {
    path: "/",
    component: require("./layouts/guest-page"),
    meta: {
      requiresGuest: true
    },
    children: [{
        path: "/login",
        component: require("./views/auth/login")
      },
      {
        path: "/password",
        component: require("./views/auth/password")
      },
      {
        path: "/register",
        component: require("./views/auth/register")
      },
      {
        path: "/distributor-register",
        component: require("./views/auth/distributorRegister")
      },
      {
        path: "/auth/:token/activate",
        component: require("./views/auth/activate")
      },
      {
        path: "/password/reset/:token",
        component: require("./views/auth/reset")
      },
      {
        path: "/auth/social",
        component: require("./views/auth/social-auth")
      }
    ]
  },
  {
    path: "*",
    component: require("./layouts/error-page"),
    children: [{
      path: "*",
      component: require("./views/errors/page-not-found")
    }]
  }
];

const router = new VueRouter({
  routes,
  linkActiveClass: "active",
  mode: "history"
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.requiresAuth)) {
    return helper.check().then(response => {
      if (!response) {
        return next({
          path: "/login"
        });
      }

      return next();
    });
  }

  if (to.matched.some(m => m.meta.requiresGuest)) {
    return helper.check().then(response => {
      if (response) {
        return next({
          path: "/"
        });
      }

      return next();
    });
  }

  return next();
});

export default router;