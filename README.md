# MLM

## List of current features
* authentication
* roles
* wallet
* transfer

``` bash
npm update
composer update
php artisan:key generate
php artisan migrate:fresh --seed
```

``` bash
npm run watch
php artisan serve
```