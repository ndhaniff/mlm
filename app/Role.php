<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{   

    /**
     * Role and User relationship
     *
     * @return User collection
     */
    public function user(){
        return $this->belongsToMany('App\User','user_role','user_id','role_id');
    }
}
