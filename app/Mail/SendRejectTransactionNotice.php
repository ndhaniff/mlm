<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendRejectTransactionNotice extends Mailable
{
    use Queueable, SerializesModels;

    public $description;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($description)
    {
        $this->description = $description;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('You transaction status on ' . date("F j, Y"))
            ->view('emails.sendRejectTransactionNotice');
    }
}
