<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendApproveTransactionNotice extends Mailable
{
    use Queueable, SerializesModels;

    public $description;
    public $amount;
    public $wallet;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $args)
    {
        $this->amount = $args['amount'];
        $this->description = $args['description'];
        $this->wallet = $args['wallet'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('You transaction status on ' . date("F j, Y"))
            ->view('emails.sendApproveTransactionNotice');
    }
}
