<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendNewDistributorPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $sender;
    public $receiver;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $args)
    {
        $this->password = $args['password'];
        $this->sender = $args['sender'];
        $this->receiver = $args['receiver'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('You account has been created by ' . $this->sender)
            ->view('emails.sendNewDistributorPassword');
    }
}
