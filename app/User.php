<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'status', 'avatar', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    /**
    * relationship between user and role
    *
    * @return Role collection
    */
    public function role(){
        return $this->belongsToMany('App\Role','user_role','role_id','user_id');
    }

    /**
    * relationship between user and role
    *
    * @return Distributor collection
    */
    public function distributor(){
        return $this->hasOne('App\Model\Distributor');
    }
}
