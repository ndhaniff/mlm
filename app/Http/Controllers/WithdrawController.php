<?php

namespace App\Http\Controllers;

use App\Model\WalletTransactionHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class WithdrawController extends Controller
{
    public function getWithdrawal(Request $request)
    {
        $withdrawal = DB::table('wallets_transaction_history')->select('*')->where('status', "!=", 'rejected');

        if ($request->query('status') != "all") {
            $withdrawal->where("status", "=", $request->query('status'));
        }
        if ($request->query('byDistributor_id') != null) {
            $withdrawal->where('distributor_id', 'like', '%' . $request->query('byDistributor_id') . '%');
        }
        if ($request->query('byDate') != null) {
            $withdrawal->where('created_at', 'like', $request->query('byDate') . '%');
        }
        if ($request->query('byAmount') != null) {
            $withdrawal->where('amount', 'like', '%' . $request->query('byAmount') . '%');
        }

        if (!$withdrawal->exists()) {
            return response()->json(["success" => false, "errors" => "no record found"]);
        }

        return response()->json(["success" => true, "data" => $withdrawal->latest()->paginate(4)]);

    }

    public function requestWithdraw(Request $request)
    {
        $wallet = DB::table('wallets as w')
            ->select('w.*', 'dw.amount')
            ->join('distributors_wallets as dw', 'dw.wallet_id', '=', 'w.id')
            ->where([
                ['dw.distributor_id', '=', $request->distributorID],
                ['w.name', '=', $this->getWalletType($request->type)],
            ]);

        $validation = $this->validateRequest($request, $wallet);

        if ($validation->fails()) {
            return response()->json(['success' => false, 'errors' => $validation->errors()], 200);
        }

        return $this->insertWithdraw($request, $wallet);

    }

    public function validateRequest($req, $wallet)
    {
        return $validator = Validator::make($req->all(), [
            'amount' => [
                'required',
                function ($attr, $value, $fail) use ($req, $wallet) {
                    if ($value) {
                        if (!$wallet->exists()) {
                            $fail('You don\'t have this type of wallet');
                        } else {
                            if ($req->amount < $wallet->first()->min_withdraw) {
                                $fail('Minimum withdraw is ' . $wallet->first()->min_withdraw);
                            } elseif ($req->amount > $wallet->first()->max_withdraw) {
                                $fail('Maximum withdraw is ' . $wallet->first()->max_withdraw);
                            } elseif ($wallet->first()->amount < $req->amount) {
                                $fail('You don\'t have enough to withdraw');
                            }
                        }
                    }
                },
            ],
        ]);
    }

    public function getWalletType($wallet)
    {
        switch ($wallet) {
            case 'pvpoint':
                return 'PV Points';
                break;

            case 'rewardspoints':
                return 'Rewards Points';
                break;

            case 'ecash':
                return 'e Cash';
                break;
        }
    }

    public function insertWithdraw($request, $wallet)
    {
        $walletAmount = DB::table('distributors_wallets as dw')
            ->where('dw.wallet_id', '=', $wallet->first()->id)
            ->update(['amount' => $wallet->first()->amount - $request->amount]);

        $walletTransaction = new WalletTransactionHistory();
        $walletTransaction->distributor_wallet_id = $wallet->first()->id;
        $walletTransaction->distributor_wallet_name = $wallet->first()->name;
        $walletTransaction->type = 'Credit';
        $walletTransaction->status = 'Pending';
        $walletTransaction->amount = $request->amount;
        $walletTransaction->distributor_id = $request->distributorID;
        $walletTransaction->description = 'Withdrawal Request';
        $walletTransaction->save();

        return response()->json(['success' => true]);
    }
}
