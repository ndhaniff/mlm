<?php
namespace App\Http\Controllers;

use App\Mail\SendApproveTransactionNotice;
use App\Model\Distributor;
use App\Model\WalletTransactionHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TransactionController extends Controller
{
    /**
     * Get History Log
     *
     * @param Request $request
     * @return Response Json
     */
    public function history(Request $request)
    {
        if ($request->query('wallet_id') == 'all') {
            $history = WalletTransactionHistory::latest()->paginate(4);
            return response()->json(['success' => true, "data" => $history], 200);
        }

        $history = WalletTransactionHistory::where([
            ['distributor_wallet_id', '=', $request->query('wallet_id')],
            ['status', '=', 'approved'],
        ]);

        if ($history->exists()) {
            return response()->json(['success' => true, "data" => $history->latest()->paginate(4)], 200);
        }

        return response()->json(['success' => false, "message" => 'no record found'], 200);
    }

    /**
     * Process Withdraw
     *
     * @param Request $request
     * @return Response Json
     */
    public function processWithdrawal(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {

            //update status in db
            $withdrawal = WalletTransactionHistory::where('id', '=', $req["id"]);

            if (!$withdrawal->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $withdrawal->update([
                "description" => $req["description"],
                "status" => 'processing',
                'updated_at' => \Carbon\Carbon::now(),
            ]);

            //Mail status
            //$email = Distributor::where('distributor_id', '=', $req['distributor_id'])->first()->email;
            // Mail::to($email)->send(new SendApproveTransactionNotice([
            //     'amount' => $req['amount'],
            //     'description' => $req["description"],
            //     'wallet' => $req['distributor_wallet_name'],
            // ]));
        }

        return response()->json(['success' => true, "message" => "Processing"]);
    }

    /**
     * Done Withdraw
     *
     * @param Request $request
     * @return Response Json
     */
    public function doneWithdrawal(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {

            //update status in db
            $withdrawal = WalletTransactionHistory::where('id', '=', $req["id"]);

            if (!$withdrawal->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $withdrawal->update([
                "description" => $req["description"],
                "status" => 'done',
                'updated_at' => \Carbon\Carbon::now(),
            ]);

            //Mail status
            //$email = Distributor::where('distributor_id', '=', $req['distributor_id'])->first()->email;
            // Mail::to($email)->send(new SendApproveTransactionNotice([
            //     'amount' => $req['amount'],
            //     'description' => $req["description"],
            //     'wallet' => $req['distributor_wallet_name'],
            // ]));
        }

        return response()->json(['success' => true, "message" => "Processing"]);
    }

    /**
     * Approve Withdraw
     *
     * @param Request $request
     * @return Response Json
     */
    public function approveWithdrawal(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {

            //update wallet
            $wallet = DB::table('distributors_wallets as dw')
                ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
                ->where([
                    ['dw.distributor_id', '=', $req['distributor_id']],
                    ['w.id', '=', $req['distributor_wallet_id']],
                ]);

            if (!$wallet->exists()) {
                return response()->json(['success' => false, "errors" => "wallet does not exist!"]);
            }

            if ('Credit' === $req['type']) {
                $wallet->decrement('dw.amount', $req["amount"]);
                $wallet->update([
                    'dw.updated_at' => \Carbon\Carbon::now(),
                    'w.updated_at' => \Carbon\Carbon::now(),
                ]);
            }

            //update status in db
            $withdrawal = WalletTransactionHistory::where('id', '=', $req["id"]);

            if (!$withdrawal->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $withdrawal->update([
                "description" => $req["description"],
                "status" => 'approved',
            ]);

            //Mail status
            $email = Distributor::where('distributor_id', '=', $req['distributor_id'])->first()->email;
            Mail::to($email)->send(new SendApproveTransactionNotice([
                'amount' => $req['amount'],
                'description' => $req["description"],
                'wallet' => $req['distributor_wallet_name'],
            ]));
        }

        return response()->json(['success' => true, "message" => "Approved!"]);
    }

    /**
     * Reject Transfer
     *
     * @param Request $request
     * @return Response Json
     */
    public function rejectWithdrawal(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {
            //update wallet
            $wallet = DB::table('distributors_wallets as dw')
                ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
                ->where([
                    ['dw.distributor_id', '=', $req['distributor_id']],
                    ['w.id', '=', $req['distributor_wallet_id']],
                ]);

            if (!$wallet->exists()) {
                return response()->json(['success' => false, "errors" => "wallet does not exist!"]);
            }

            if ('Credit' === $req['type']) {
                $wallet->increment('dw.amount', $req["amount"]);
                $wallet->update([
                    'dw.updated_at' => \Carbon\Carbon::now(),
                    'w.updated_at' => \Carbon\Carbon::now(),
                ]);
            }

            $withdrawal = WalletTransactionHistory::where('id', '=', $req["id"]);

            if (!$withdrawal->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $withdrawal->update([
                "description" => $req["description"],
                "status" => 'rejected',
                "updated_at" => \Carbon\Carbon::now(),
            ]);

            // $email = Distributor::where('distributor_id', '=', $req['distributor_id'])->first()->email;

            // Mail::to($email)->send(new SendRejectTransactionNotice($req["description"]));
        }

        return response()->json(['success' => true, "message" => "Rejected!"]);
    }
}
