<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Wallet;

class WalletController extends Controller
{
    /**
     * Get all user wallet
     *
     * @return void
     */
    public function index(Request $request){

        if(request()->has('user_id')) {    
            $wallet = DB::table('distributors_wallets')
                        ->select('wallets.*', 'distributors_wallets.amount')
                        ->join('users', 'users.id', '=' , 'distributors_wallets.user_id')
                        ->leftJoin('wallets', 'wallets.id', '=', 'distributors_wallets.wallet_id')
                        ->where('users.id', '=', $request->user_id);
        }
        
        return $wallet->get();
    }

    public function getWallet(Request $request)
    {   
        $wallet = Wallet::where('id', '=', $request->id);
        
        if($wallet->exists()){

            return response()->json(['success' => true, 'data' => $wallet->first()] ,200);
        }

        return response()->json(['success' => false], 200);
    }


}
