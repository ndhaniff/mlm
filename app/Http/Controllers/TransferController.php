<?php

namespace App\Http\Controllers;

use App\Mail\SendApproveTransactionNotice;
use App\Mail\SendRejectTransactionNotice;
use App\Model\Distributor;
use App\Model\Transfer;
use App\Model\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class TransferController extends Controller
{
    public function getTransfer(Request $request)
    {
        $withdrawal = DB::table('wallets_transfer')->select('*')->where('status', "!=", 'rejected');

        if ($request->query('status') != "all") {
            $withdrawal->where("status", "=", $request->query('status'));
        }
        if ($request->query('toDistributor') != null) {
            $withdrawal->where('distributor_id_destination', 'like', '%' . $request->query('toDistributor') . '%');
        }
        if ($request->query('byDate') != null) {
            $withdrawal->where('created_at', 'like', $request->query('byDate') . '%');
        }
        if ($request->query('fromDistributor') != null) {
            $withdrawal->where('distributor_id_source', 'like', '%' . $request->query('fromDistributor') . '%');
        }

        if (!$withdrawal->exists()) {
            return response()->json(["success" => false, "errors" => "no record found"]);
        }

        return response()->json(["success" => true, "data" => $withdrawal->latest()->paginate(4)]);

    }

    /**
     * Get Transfer Request (Admin)✌
     *
     * @param Request $request
     * @return JSON response
     */
    public function getTransferRequest(Request $request)
    {
        $transfer = DB::table('wallets_transfer as wt')
            ->select('wt.*', 'w.name as distributor_wallet_name_source', 'w2.name as distributor_wallet_name_destination')
            ->join('wallets as w', 'wt.distributor_wallet_id_source', '=', 'w.id')
            ->join('wallets as w2', 'wt.distributor_wallet_id_destination', '=', 'w2.id')
            ->where('wt.status', '=', $request->query('status'));

        if (!$transfer->exists()) {
            return response()->json(['success' => false, 'message' => 'no record found'], 200);
        }

        return response()->json(['success' => true, 'data' => $transfer->latest()->paginate(5)], 200);
    }
    /**
     * Create Transfer Request ✌
     *
     * @param Request $request
     * @return JSON response
     */
    public function requestTransfer(Request $request)
    {
        //From
        $fromId = explode(' ', $request->from_distributor_id);
        $fromWallet = DB::table('distributors_wallets as dw')
            ->select('dw.id', 'dw.amount', 'w.*')
            ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
            ->where([
                ['dw.distributor_id', '=', $fromId[0]],
                ['w.name', '=', $this->getWalletType($request->from_wallet)],
            ]);

        //Destination
        $toId = explode(' ', $request->to_distributor_id);
        $toWallet = DB::table('distributors_wallets as dw')
            ->select('dw.id', 'dw.amount', 'w.*')
            ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
            ->where([
                ['dw.distributor_id', '=', $toId[0]],
                ['w.name', '=', $this->getWalletType($request->to_wallet)],
            ]);

        $distributor_to = Distributor::where('distributor_id', '=', $toId[0]);

        $validation = $this->validateRequest($request, $distributor_to, $fromWallet);

        if ($validation->fails()) {
            return response()->json(['success' => false, 'errors' => $validation->errors()], 200);
        }

        if (!$fromWallet->exists()) {
            return response()->json(['success' => true, 'errors' => ['You does not have this type of wallet']], 200);
        }

        if (!$toWallet->exists()) {
            return response()->json(['success' => false, 'errors' => ['The "To" distributor does not have this type of wallet']], 200);
        }

        return $this->insertTransfer($request, $fromWallet->first(), $toWallet->first());
    }

    /**
     * Get transfer request log
     *
     * @param Request $request
     * @return void
     */
    public function getTransferHistoryLog(Request $request)
    {
        if ("all" != $request->type) {

            $transferRecord = DB::table('wallets_transfer as wt')
                ->select('wt.*')
                ->where([
                    ['wt.distributor_id_source', 'like', $request->distributorID . '%'],
                    ['wt.distributor_wallet_id_source', '=', $request->walletID],
                ]);

            if ($transferRecord->exists()) {
                return response()->json(['success' => true, 'data' => $transferRecord->latest()->paginate(4)], 200);
            }

            return response()->json(['success' => false, 'errors' => 'no data found'], 200);
        } else {
            $transferRecord = DB::table('wallets_transfer')
                ->select('*')
                ->where('distributor_id_source', 'like', $request->distributorID . '%');

            if ($transferRecord->exists()) {
                return response()->json(['success' => true, 'data' => $transferRecord->latest()->paginate(4)], 200);
            }

            return response()->json(['success' => false, 'errors' => 'no data found'], 200);
        }

    }

    /**
     * Validate transfer request
     *
     * @param Request $req
     * @param Distributor $distributor_to
     * @return void
     */
    public function validateRequest($req, $distributor_to, $fromWallet)
    {
        return Validator::make($req->all(), [
            'from_distributor_id' => 'required',
            'to_distributor_id' => 'required',
            'from_wallet' => 'required',
            'to_wallet' => 'required',
            'amount' => [
                'required',
                'integer',
                function ($attr, $value, $fail) use ($fromWallet, $req) {
                    if ($value) {
                        if ($fromWallet->exists()) {
                            if ($req->amount < $fromWallet->first()->min_transfer) {
                                $fail('Minimum transfer is ' . $fromWallet->first()->min_transfer);
                            } elseif ($req->amount > $fromWallet->first()->max_transfer) {
                                $fail('Maximum transfer is ' . $fromWallet->first()->max_transfer);
                            } elseif ($fromWallet->first()->amount < $req->amount) {
                                $fail('You don\'t have enough to transfer');
                            }
                        } else {
                            $fail('You don\'t have this type of wallet');
                        }
                    }
                },
            ],
        ]);
    }

    /**
     * Get Wallet Actual Name
     *
     * @param Request $wallet
     * @return void
     */
    public function getWalletType($wallet)
    {
        switch ($wallet) {
            case 'pvpoint':
                return 'PV Points';
                break;

            case 'rewardspoints':
                return 'Rewards Points';
                break;

            case 'ecash':
                return 'e Cash';
                break;
        }
    }

    /**
     * Insert Transfer record into DB
     *
     * @param Request $request
     * @param Number $amount
     * @return void
     */
    public function insertTransfer($request, $fromWallet, $toWallet)
    {
        try {
            $toName = Distributor::where('distributor_id', '=', $request->to_distributor_id);

            $transfer = new Transfer();
            $transfer->distributor_wallet_id_source = $fromWallet->id;
            $transfer->distributor_wallet_id_destination = $toWallet->id;
            $transfer->distributor_wallet_name_source = $fromWallet->name;
            $transfer->distributor_wallet_name_destination = $toWallet->name;
            $transfer->distributor_id_source = $request->from_distributor_id;
            $transfer->distributor_id_destination = $request->to_distributor_id . " " . $toName->first()->name;
            $transfer->amount_source = $request->amount;
            $transfer->amount_destination = $toWallet->amount;
            $transfer->status = 'pending';
            $transfer->save();

            DB::table('distributors_wallets')
                ->where('wallet_id', $fromWallet->id)
                ->update(['amount' => $fromWallet->amount - $request->amount]);

            return response()->json(['success' => true]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()], 200);
        }

    }

    /**
     * Approve Transfer Request
     *
     * @param Request $request
     * @return void
     */
    public function approveTransfer(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {
            $transfer = Transfer::where('id', '=', $req["id"]);

            $desWallet = DB::table('distributors_wallets as dw')
                ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
                ->where([
                    ['dw.distributor_id', '=', explode(' ', $req['distributor_id_destination'])[0]],
                    ['w.id', '=', $req['distributor_wallet_id_destination']],
                ]);

            if (!$desWallet->exists()) {
                return response()->json(['success' => false, "errors" => "wallet does not exist!"]);
            }

            if (!$transfer->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $transfer->update([
                "remark" => $req["remark"],
                "status" => 'approved',
            ]);

            //increase amount on destination wallet
            $desWallet->increment('dw.amount', $req["amount_destination"]);
            $desWallet->update([
                'dw.updated_at' => \Carbon\Carbon::now(),
                'w.updated_at' => \Carbon\Carbon::now(),
            ]);

            $email = Distributor::where('distributor_id', '=', explode(' ', $req['distributor_id_source'])[0])->first()->email;

            Mail::to($email)->send(new SendApproveTransactionNotice($req["remark"]));
        }

        return response()->json(['success' => true, "message" => "Approved!"]);
    }

    /**
     * Reject Transfer Request
     *
     * @param Request $request
     * @return void
     */
    public function rejectTransfer(Request $request)
    {
        if (!is_array($request->all())) {
            return response()->json(['success' => false, "errors" => "invalid request!"]);
        }

        foreach ($request->all() as $req) {
            $transfer = Transfer::where('id', '=', $req["id"]);

            $sourceWallet = DB::table('distributors_wallets as dw')
                ->join('wallets as w', 'dw.wallet_id', '=', 'w.id')
                ->where([
                    ['dw.distributor_id', '=', explode(' ', $req['distributor_id_source'])[0]],
                    ['w.id', '=', $req['distributor_wallet_id_source']],
                ]);

            if (!$sourceWallet->exists()) {
                return response()->json(['success' => false, "errors" => "wallet does not exist!"]);
            }

            if (!$transfer->exists()) {
                return response()->json(['success' => false, "errors" => "record does not exist!"]);
            }

            $transfer->update([
                "remark" => $req["remark"],
                "status" => 'rejected',
            ]);

            //refund amount on source wallet
            $sourceWallet->increment('dw.amount', $req["amount_source"]);
            $sourceWallet->update([
                'dw.updated_at' => \Carbon\Carbon::now(),
                'w.updated_at' => \Carbon\Carbon::now(),
            ]);

            $email = Distributor::where('distributor_id', '=', explode(' ', $req['distributor_id_source'])[0])->first()->email;

            Mail::to($email)->send(new SendRejectTransactionNotice($req["remark"]));
        }

        return response()->json(['success' => true, "message" => "Rejected!"]);
    }
}
