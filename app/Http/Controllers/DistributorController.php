<?php

namespace App\Http\Controllers;

use App\Mail\SendNewDistributorPassword;
use App\Model\Distributor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class DistributorController extends Controller
{
    /**
     * Register distributor
     *
     * @param Request $request
     * @return Response json
     */
    public function register(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'upline_distributor_id' => ['required', 'regex:/D[a-zA-Z0-9]{3}/g'],
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
            'nationality' => 'required',
            'passport_id' => 'nullable',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 200);
        }

        return response()->json(['success' => true]);

    }

    /**
     * Get distributor detail
     *
     * @param Request $request
     * @return Response json
     */
    public function getDistributor(Request $request)
    {

        if (!$request->has('id') && $request->has('status')) {

            $distributor = \App\Model\Distributor::select('distributors.*', 'p.name as package_name')
                ->join('packages as p', 'distributors.package_id', '=', 'p.id')
                ->where('distributors.status', '=', $request->query('status'))
                ->orderBy('distributors.created_at', 'asc');

            if ($request->query('byDistributor_id') != "") {
                $distributor->where('distributors.distributor_id', 'like', '%' . $request->query('byDistributor_id') . '%');
            }
            if ($request->query('byDate') != "") {
                $distributor->where('distributors.created_at', 'like', '%' . $request->query('byDate') . '%');
            }
            if ($request->query('byEmail') != "") {
                $distributor->where('distributors.email', 'like', '%' . $request->query('byEmail') . '%');
            }

            return response()->json(['success' => true, 'data' => $distributor->latest()->paginate(4)], 200);
        }

        $distributor = DB::table('distributors as d')
            ->select(
                'd.*',
                'p.name as package_name',
                'p.comission_point',
                'p.comission_level',
                'db.bank_name',
                'db.account_number',
                'db.beneficiary_name',
                'db.beneficiary_email',
                'db.beneficiary_phone'
            )
            ->join('packages as p', 'p.id', '=', 'd.package_id')
            ->leftJoin('distributors_bank as db', 'db.distributor_id', '=', 'd.distributor_id');

        if (!$request->has('did')) {
            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 200);
            }

            $distributor->where('d.id', '=', $request->id);
        } else {
            $validator = Validator::make($request->all(), [
                'did' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 200);
            }

            $distributor->where('d.distributor_id', '=', $request->did);
        }

        if (!$distributor->exists()) {
            return response()->json(['success' => false, 'errors' => ['this distributor does not exist']], 200);
        }

        return response()->json(['success' => true, 'data' => $distributor->first()], 200);

    }

    /**
     * Add Distributor to the DB
     *
     * @param Request $request
     * @return Response Json
     */
    public function addDistributor(Request $request)
    {

        $validation = Validator::make($request->all(), [
            "personalDetails.fullName" => "required",
            "personalDetails.email" => "required|email",
            "personalDetails.contact" => "required",
            "personalDetails.icnum" => "required",
            "personalDetails.nationality" => "required",
            "packageDetails.package" => "required",
            "bankDetails.bankName" => "required",
            "bankDetails.accnum" => "required",
            "bankDetails.beneficiaryName" => "required",
            "bankDetails.contact" => "required",
        ]);

        if ($validation->fails()) {
            return response()->json(['success' => false, 'errors' => $validation->errors()], 200);
        }

        $user = \App\User::where('email', '=', $request->personalDetails['email']);

        $did = "D" . strtoupper(str_random(3));
        $tempPass = str_random(8);
        $lastDownlineId = $this->getLastDownline($request->upline["path"], $request->upline['id']);

        if ($user->exists()) {

            $distributor = \App\Model\Distributor::where('user_id', '=', $user->first()->id);

            if ($distributor->exists()) {
                return response()->json(['success' => false, 'errors' => ['distributor with this email exist']], 200);
            }

            $newDistributor = DB::table('distributors')
                ->insert([
                    "distributor_id" => $did,
                    "user_id" => $user->first()->id,
                    "name" => $request->personalDetails["fullName"],
                    "email" => $request->personalDetails['email'],
                    "phone_number" => $request->personalDetails['contact'],
                    "id_passport" => $request->personalDetails['icnum'],
                    "nationality" => $request->personalDetails['nationality'],
                    "package_id" => $request->packageDetails['package'],
                    "upline_distributor_id" => $request->upline['id'],
                    "before_distributor_id" => $this->getLastDownline($request->upline["path"], $request->upline['id']),
                    "upline_path" => $request->upline["path"],
                    "status" => "pending",
                    "distributor_path" => $request->upline["id"] != $lastDownlineId ? $request->upline["upline_path"] . "-" . $request->upline["id"] . "-" . $lastDownlineId : $request->upline["upline_path"] . "-" . $request->upline["id"],
                    "created_at" => \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]);

            if (!$newDistributor) {
                return response()->json(['success' => false, 'errors' => ['an error occured 2']], 200);
            }

            return response()->json(['success' => true, 'message' => 'user created!'], 200);
        }

        //else create user with distributor acc and password will be email to user

        $newUser = DB::table('users')
            ->insert([
                "email" => $request->personalDetails['email'],
                "password" => bcrypt($tempPass),
                "status" => "activated",
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);

        $newUserId = DB::getPdo()->lastInsertId();

        if (null == $newUserId) {
            return response()->json(['success' => false, 'errors' => ['an error occured 3']], 200);
        }

        $newDistributor = DB::table('distributors')
            ->insert([
                "distributor_id" => $did,
                "user_id" => $newUserId,
                "name" => $request->personalDetails["fullName"],
                "email" => $request->personalDetails['email'],
                "phone_number" => $request->personalDetails['contact'],
                "id_passport" => $request->personalDetails['icnum'],
                "nationality" => $request->personalDetails['nationality'],
                "package_id" => $request->packageDetails['package'],
                "upline_distributor_id" => $request->upline['id'],
                "before_distributor_id" => $this->getLastDownline($request->upline["path"], $request->upline['id']),
                "upline_path" => $request->upline["path"],
                "status" => "pending",
                "distributor_path" => $request->upline["id"] != $lastDownlineId ? $request->upline["upline_path"] . "-" . $request->upline["id"] . "-" . $lastDownlineId : $request->upline["upline_path"] . "-" . $request->upline["id"],
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);

        Mail::to($request->personalDetails["email"])->send(new SendNewDistributorPassword([
            'password' => $tempPass,
            'receiver' => $request->upline["email"],
            'sender' => $request->upline["name"],
        ]));

        return response()->json(['success' => true, 'message' => 'user created!'], 200);

    }

    /**
     * Update Distributor
     *
     * @param Request $request
     * @return Response Json
     */
    public function updateDistributor(Request $request)
    {
        $lastDownlineId = $this->getLastDownline($request->path, $request->packageDetails['directupline']);
        $uplinepath = Distributor::where('distributor_id', '=', $request->packageDetails['directupline']);
        $distributor = DB::table('distributors as d')
            ->join('distributors_bank as db', 'd.distributor_id', '=', 'db.distributor_id')
            ->where('d.distributor_id', "=", $request->personalDetails['distributor_id']);

        if (!$uplinepath->exists()) {
            return response()->json(['success' => true, 'errors' => 'upline user does not exist!'], 200);
        }

        if (!$distributor->exists()) {
            return response()->json(['success' => false, 'errors' => 'distributor does not exist!'], 200);
        }
        $uplinepath = $uplinepath->first()->distributor_path;
        $dPath = $lastDownlineId == null ? $uplinepath . "-" . $request->packageDetails['directupline'] : $uplinepath . "-" . $request->packageDetails['directupline'] . "-" . $lastDownlineId;
        $distributor->update([
            "d.name" => $request->personalDetails["name"],
            "d.email" => $request->personalDetails['email'],
            "d.phone_number" => $request->personalDetails['contact'],
            "d.id_passport" => $request->personalDetails['icnum'],
            "d.nationality" => $request->personalDetails['nationality'],
            "d.package_id" => $request->packageDetails['package'],
            "d.upline_distributor_id" => $request->packageDetails['directupline'],
            "d.before_distributor_id" => null != $lastDownlineId ? $lastDownlineId : "",
            "d.upline_path" => $request->path,
            "d.status" => $request->status,
            "d.distributor_path" => $request->packageDetails['directupline'] != $lastDownlineId ? $dPath : $uplinepath . "-" . $request->packageDetails['directupline'],
            "db.bank_name" => $request->bankDetails['bankname'],
            "db.account_number" => $request->bankDetails['accnum'],
            "db.beneficiary_name" => $request->bankDetails['beneficiaryname'],
            "db.beneficiary_email" => $request->bankDetails['email'],
            "db.beneficiary_phone" => $request->bankDetails['contact'],
            "d.updated_at" => \Carbon\Carbon::now(),
        ]);

        if (!$distributor > 0) {
            return response()->json(['success' => false, 'errors' => 'error when updating!'], 200);
        }

        return response()->json(['success' => true, 'message' => 'user updated!'], 200);

    }

    /**
     * Get Downline
     *
     * @param Request $request
     * @return Response Json
     */
    public function getDownline(Request $request)
    {
        if (!$request->has('distributor_id')) {
            return response()->json(['success' => false, 'errors' => ['Please provide the id']], 200);
        }
        if (!$request->has('path')) {
            return response()->json(['success' => false, 'errors' => ['Please provide the path']], 200);
        }

        $downline = DB::table('distributors')
            ->where([
                ['upline_distributor_id', '=', $request->distributor_id],
                ['upline_path', '=', $request->path],
            ]);

        if (!$downline->exists()) {
            return response()->json(['success' => false, 'errors' => ['Record does not exist']], 200);
        }

        return response()->json(['success' => true, 'data' => $downline->get()], 200);
    }

    public function getLastDownline(string $path, string $id)
    {
        $downline = DB::table('distributors')
            ->orderBy('created_at', 'asc')
            ->where([
                ['upline_distributor_id', '=', $id],
                ['upline_path', '=', $path],
            ]);

        if (!$downline->exists()) {
            return;
        }

        return $downline->get()->last()->distributor_id;
    }

    public function getLastPathId(string $path)
    {
        $pathArray = explode('-', $path);
        return end($pathArray);
    }
}
