<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WalletTransactionHistory extends Model
{
    protected $table = "wallets_transaction_history";
}
