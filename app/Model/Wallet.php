<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
     * Wallet and Distributor Relationship
     *
     * @return Wallet
     */
    public function distributor(){
        return $this->belongsToMany('App\Model\Distributor','distributors_wallets');
    }
    
    public function transfer(){
        return $this->hasMany('App\Model\Transfer');
    }
}
