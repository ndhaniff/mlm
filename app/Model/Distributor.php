<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{   
    /**
     * Distributor and User relationship
     *
     * @return User
     */
    public function user(){
        return $this->belongsToMany('App\User');
    }

    /**
     * Distributor and Wallet relationship
     *
     * @return Wallet
     */
    public function wallet(){
        return $this->belongsToMany('App\Model\Wallet','distributors_wallets');
    }
}
