<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{   
    protected $table = "wallets_transfer";

    public function wallet(){
        return $this->belongsTo('App\Model\Wallet');
    }
}
